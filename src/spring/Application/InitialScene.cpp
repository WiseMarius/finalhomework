#include "..\..\..\include\spring\Application\InitialScene.h"
#include <qapplication.h>
#include <iostream>

namespace Spring
{
	InitialScene::InitialScene(const std::string & ac_szSceneName) : IScene(ac_szSceneName)
	{
	}

	void InitialScene::createScene()
	{
		createGUI();

		QObject::connect(plotPushButton, SIGNAL(released()), this, SLOT(mf_plotPushButton()));
	}

	void InitialScene::release()
	{
		delete centralWidget;
	}

	InitialScene::~InitialScene()
	{
	}

	void InitialScene::createGUI()
	{
		m_uMainWindow.get()->resize(700, 400);
		centralWidget = new QWidget(m_uMainWindow.get());
		centralWidget->setObjectName(QStringLiteral("centralWidget"));
		gridLayout_2 = new QGridLayout(centralWidget);
		gridLayout_2->setSpacing(6);
		gridLayout_2->setContentsMargins(11, 11, 11, 11);
		gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
		gridLayout = new QGridLayout();
		gridLayout->setSpacing(6);
		gridLayout->setObjectName(QStringLiteral("gridLayout"));
		frequencyLabel = new QLabel(centralWidget);
		frequencyLabel->setObjectName(QStringLiteral("frequencyLabel"));

		gridLayout->addWidget(frequencyLabel, 2, 1, 1, 1);

		plotPushButton = new QPushButton(centralWidget);
		plotPushButton->setObjectName(QStringLiteral("plotPushButton"));

		gridLayout->addWidget(plotPushButton, 2, 4, 1, 1);

		refreshRateLineEdit = new QLineEdit(centralWidget);
		refreshRateLineEdit->setObjectName(QStringLiteral("frequencyLineEdit"));

		gridLayout->addWidget(refreshRateLineEdit, 2, 2, 1, 1);

		horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

		gridLayout->addItem(horizontalSpacer, 2, 0, 1, 1);

		plot = new QCustomPlot(centralWidget);
		plot->setObjectName(QStringLiteral("plot"));

		gridLayout->addWidget(plot, 0, 0, 1, 5);


		gridLayout_2->addLayout(gridLayout, 0, 0, 1, 1);

		m_uMainWindow.get()->setCentralWidget(centralWidget);
		menuBar = new QMenuBar(m_uMainWindow.get());
		menuBar->setObjectName(QStringLiteral("menuBar"));
		menuBar->setGeometry(QRect(0, 0, 700, 25));
		m_uMainWindow.get()->setMenuBar(menuBar);
		statusBar = new QStatusBar(m_uMainWindow.get());
		statusBar->setObjectName(QStringLiteral("statusBar"));
		m_uMainWindow.get()->setStatusBar(statusBar);

		m_uMainWindow.get()->setWindowTitle(QApplication::translate("MainWindow", "DisplayWindow", Q_NULLPTR));
		frequencyLabel->setText(QApplication::translate("MainWindow", "Refresh rate", Q_NULLPTR));
		plotPushButton->setText(QApplication::translate("MainWindow", "Plot", Q_NULLPTR));
	}

	void InitialScene::mf_plotPushButton()
	{
		QVector<double> xAxis;
		QVector<double> yAxis;

		plot->addGraph();
		int iter = 0;

		SerialPort arduino(port_name);
		if (arduino.isConnected())
			std::cout << "Connection Established" << endl;
		else
			std::cout << "ERROR, check port name";

		while (arduino.isConnected())
		{
			//Check if data has been read or not
			int read_result = arduino.readSerialPort(incomingData, MAX_DATA_LENGTH);

			xAxis.push_back(iter);
			yAxis.push_back(atof(incomingData));

			if (iter == 10)		//freq hardcodat
			{
				plot->graph(0)->setData(xAxis, yAxis);
				plot->rescaleAxes();
				plot->replot();

				iter = 0;

				xAxis.erase(xAxis.begin(), xAxis.end());
				yAxis.erase(yAxis.begin(), yAxis.end());

				Sleep(1000 * refreshRateLineEdit->text().toDouble());	//timpul de asteptare depinde totusi si de frecventa... ar mai fi calcule de facut 
															//pentru ca acest refreshrate specificat de utilizator sa fie exact.
															//O limita minima ar trbui de asemenea impusa deoarece senzorul de pe arduino
															//ofera un anumit numar de valori pe secunda => valoarea minima a refreshrateului = 
															//1 / frecventa senzorului.
															//Interfata primeste freeze => ar trebui un thread separat pentru calculul xAxis si yAxis.
															//Sunt sigur ca nu este cea mai buna implementare, dar e singura care mi-a venit in minte
															//la 4am.
			}
			iter++;
		}
	}
}

