#pragma once
#include <spring\Framework\IScene.h>
#include <qgridlayout.h>
#include <qspinbox.h>
#include <qpushbutton.h>
#include <qlabel>
#include <qlineedit.h>
#include <qobject.h>
#include <qcustomplot.h>
#include <SerialPort.h>

namespace Spring
{
	class InitialScene : public IScene
	{
		Q_OBJECT

	public:

		explicit InitialScene(const std::string& ac_szSceneName);

		void createScene() override;

		void release() override;

		~InitialScene();

	private:
		void createGUI();

		char *port_name = "\\\\.\\COM1";
		char incomingData[MAX_DATA_LENGTH];

		QWidget *centralWidget;
		QGridLayout *gridLayout_2;
		QGridLayout *gridLayout;
		QLabel *frequencyLabel;
		QPushButton *plotPushButton;
		QLineEdit *refreshRateLineEdit;
		QSpacerItem *horizontalSpacer;
		QCustomPlot *plot;
		QMenuBar *menuBar;
		QStatusBar *statusBar;

		private slots:
		void mf_plotPushButton();

	};
}
