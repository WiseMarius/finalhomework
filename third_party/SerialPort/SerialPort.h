#pragma once

#ifdef SerialPort_EXPORTS
	#define SerialPort_API __declspec(dllexport)
#else
	#define SerialPort_API __declspec(dllimport)
#endif

#ifndef SERIALPORT_H
	#define SERIALPORT_H

#include <iostream>

#define ARDUINO_WAIT_TIME 2000
#define MAX_DATA_LENGTH 255

#include <windows.h>

class SerialPort_API SerialPort
{
private:
	HANDLE handler;
	bool connected;
	COMSTAT status;
	DWORD errors;
public:
	SerialPort(char *portName);
	~SerialPort();

	int readSerialPort(char *buffer, unsigned int buf_size);
	bool writeSerialPort(char *buffer, unsigned int buf_size);
	bool isConnected() const;
};

#endif // SERIALPORT_H